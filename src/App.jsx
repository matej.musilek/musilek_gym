import React from "react";
import Home from "./pages/home/home";
import TeamOndra from "./pages/team/TeamOndra";
import "./main.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import TeamMate from "./pages/team/TeamMate";
import TeamJan from "./pages/team/TeamJan";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/team/ondra" element={<TeamOndra />} />
        <Route path="/team/mata" element={<TeamMate />} />
        <Route path="/team/jan" element={<TeamJan />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
