import React from "react";
import $ from "jquery";
import "./home.css";
import "./media.css";

import Navbar from "../../components/navbar/navbar";
import Footer from "../../components/footer/footer";
import Contant from "../../components/contact/contact";
import { Link } from "react-router-dom";
import emailjs from "@emailjs/browser";

export default function Home() {
  return (
    <>
      <Navbar />
      <section id="home-sec">
        <div className="home-page">
          <div className="container">
            <div className="home-title">
              <h1>
                MAKEJTE S <span className="title-color">PROFESIONÁLY</span>
              </h1>
              <p>Vítejte v Musílek Gym, kde spojujeme vášeň pro pohyb s profesionálním přístupem ke každému cviku.</p>
            </div>
            <div className="home-btn">
              <div className="row">
                <div className="col-md-6">
                  <div className="home-btn-sluzby">
                    <a href="#sluzby">Služby</a>
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="home-btn-about">
                    <a href="#about">O Nás</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="benefits">
        <div className="container">
          <div className="col-12">
            <div className="benefits-title">
              <h1>PROČ SI VYBRAT NÁS?</h1>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/muscle 1.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Budování Sválů</h1>
                  <p>V Musílek Gym jdeme dál než jen k budování svalů – společně formujeme sílu, výdrž a energii.</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/help 1.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Profesionální přístup</h1>
                  <p>V Musílek Gym se neomezujeme jen na cvičení, poskytujeme profesionální přístup ke každému detailu vašeho fitness journey. S námi nejen cvičíte, ale také dosahujete maximálních výsledků s individuálním podhledem od našich trenérů.</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/automatic 1.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Moderní vybavení</h1>
                  <p>V Musílek Gym spojujeme moderní vybavení s vášní pro fitness. Získejte optimální tréninkový zážitek s nejnovější technologií a formujte své tělo s pohodlím a efektivitou.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="coach">
        <div className="container">
          <div className="col-12">
            <div className="coach-title">
              <h1>NAŠI TRENÉŘI</h1>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/kler 3.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Ondřej Klér</h1>
                  <p>Jsem trenér se zaměřením na silový trénink. Mým cílem je pomoci vám dosáhnout optimální fyzické kondice s důrazem na správnou techniku cvičení a individuální plánování programu.</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/musilek 3.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Matěj Musílek</h1>
                  <p>Jsem majitel a crossfit trenér, kombinuje kondici, sílu a flexibility. Mým úkolem je vás motivovat a posunout vaše fyzické schopnosti na vyšší úroveň.</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/vitek 3.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Jan Vítek</h1>
                  <p>Můj hlavní úkol spočívá ve sestavování jídelníčků, které nejen podpoří váš trénink, ale také vám pomohou dosáhnout vašich fitness cílů.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="predplatne">
        <div className="container">
          <div className="col-12">
            <div className="predplatne-title">
              <h1>NAŠE PŘEDPLATNÉ</h1>
              <p>Dospelý / Student</p>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-sm-12">
              <div className="predplatne-content">
                <h1 className="yellow">Měsíční předplatné</h1>
                <p>1050 Kč / 950 Kč</p>
              </div>
            </div>
            <div className="col-md-6  col-sm-12">
              <div className="predplatne-content">
                <h1>3 Měsíční predplatné</h1>
                <p>2950 Kč / 2550 Kč</p>
              </div>
            </div>
            <div className="vl"></div>
            <div className="col-md-6  col-sm-12">
              <div className="predplatne-content">
                <h1>6 Měsíční předplatné</h1>
                <p>5050 Kč / 4300 Kč</p>
              </div>
            </div>
            <div className="col-md-6  col-sm-12">
              <div className="predplatne-content">
                <h1 className="yellow">Jednorázový vstup</h1>
                <p>125 Kč / 100 Kč</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="about">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="about-content">
                <h1>O NÁS</h1>
                <p>
                  Jsme prestižní gym , nacházející se na ulici Na Hradbách 11, 280 02, v malebném městě Kolín. Zakladatelé a zároveň hlavní trenéři této moderní posilovny jsou <span className="yellow">Ondřej Klér, Matěj Musílek a Jan Vítek.</span>
                </p>
                <a href="#about-page-content">Číst Více</a>
              </div>
            </div>
            <div className="col-md-8">
              <img src="/images/risen-wang-20jX9b35r_M-unsplash 2.png" alt="" />
              <div className="img-content">
                <img className="mini-pic" src="/images/evan-wise-wTcD3MwL_VY-unsplash 2.png" alt="" />
                <img className="mini-pic" src="/images/scott-webb-U5kQvbQWoG0-unsplash 2.png" alt="" />
                <img className="mini-pic" src="/images/sven-mieke-MsCgmHuirDo-unsplash 9.png" alt="" />
              </div>
              <a className="gallery-odkaz" href="#gallery">
                Zobrazit Galerii
              </a>
            </div>
          </div>
        </div>
      </section>
      <section id="about-page">
        <div className="page-title">
          <h1>O NÁS</h1>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div id="about-page-content">
                <p>
                  Jsme prestižní gym , nacházející se na ulici Na Hradbách 11, 280 02, v malebném městě Kolín. Zakladatelé a zároveň hlavní trenéři této moderní posilovny jsou
                  <span className="yellow"> Ondřej Klér, Matěj Musílek a Jan Vítek. </span>
                  Společně přinášejí do fitness komunity své bohaté zkušenosti, odborné know-how a nadšení pro zdravý životní styl.
                  <br /> Musílek Gym se vyznačuje nejen kvalitním vybavením, ale také přátelskou atmosférou a individuálním přístupem ke každému členovi. Tým trenérů je odhodlán pomoci klientům dosáhnout jejich fitness cílů a podporovat je na cestě ke zlepšení fyzické kondice.
                </p>
              </div>
            </div>
            <div className="about-page-img">
              <div className="col-md-4 col-sm-12">
                <Link to="/team/ondra">
                  <img src="/images/kler 3.png" alt="" />
                  <h1>Ondřej Klér</h1>
                </Link>
              </div>
              <div className="col-md-4 col-sm-12">
                <Link to="/team/mata">
                  <img src="/images/musilek 3.png" alt="" />
                  <h1>Matěj Musílek</h1>
                </Link>
              </div>
              <div className="col-md-4 col-sm-12">
                <Link to="/team/jan">
                  <img src="/images/vitek 3.png" alt="" />
                  <h1>Jan Vítek</h1>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="gallery">
        <div className="page-title">
          <h1>GALERIE</h1>
        </div>
        <div className="gallery-content">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/brett-jordan-U2q73PfHFpM-unsplash 1.png">
                  <img src="/images/gallery/brett-jordan-U2q73PfHFpM-unsplash 1.png" alt="" />
                </a>
              </div>
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/daniel-apodaca-WdoQio6HPVA-unsplash 1.png">
                  <img src="/images/gallery/daniel-apodaca-WdoQio6HPVA-unsplash 1.png" alt="" />
                </a>
              </div>
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/danielle-cerullo-CQfNt66ttZM-unsplash 1.png">
                  <img src="/images/gallery/danielle-cerullo-CQfNt66ttZM-unsplash 1.png" alt="" />
                </a>
              </div>
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/evan-wise-wTcD3MwL_VY-unsplash 1.png">
                  <img src="/images/gallery/evan-wise-wTcD3MwL_VY-unsplash 1.png" alt="" />
                </a>
              </div>
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/risen-wang-20jX9b35r_M-unsplash 1.png">
                  <img src="/images/gallery/risen-wang-20jX9b35r_M-unsplash 1.png" alt="" />
                </a>
              </div>
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/samuel-girven-VJ2s0c20qCo-unsplash 1.png">
                  <img src="/images/gallery/samuel-girven-VJ2s0c20qCo-unsplash 1.png" alt="" />
                </a>
              </div>
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/scott-webb-U5kQvbQWoG0-unsplash 1.png">
                  <img src="/images/gallery/scott-webb-U5kQvbQWoG0-unsplash 1.png" alt="" />
                </a>
              </div>
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/sven-mieke-MsCgmHuirDo-unsplash 5.png">
                  <img src="/images/gallery/sven-mieke-MsCgmHuirDo-unsplash 5.png" alt="" />
                </a>
              </div>
              <div className="col-md-4">
                <a target="_blank" href="/images/gallery/victor-freitas-Yuv-iwByVRQ-unsplash 1.png">
                  <img src="/images/gallery/victor-freitas-Yuv-iwByVRQ-unsplash 1.png" alt="" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="sluzby">
        <div className="page-title">
          <h1>SLUŽBY</h1>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="sluzby-title">
                <h1>Osobní trenéři</h1>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/kler 3.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Ondřej Klér</h1>
                  <p>Jsem trenér se zaměřením na silový trénink. Mým cílem je pomoci vám dosáhnout optimální fyzické kondice s důrazem na správnou techniku cvičení a individuální plánování programu.</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/musilek 3.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Matěj Musílek</h1>
                  <p>Jsem majitel a crossfit trenér, kombinuje kondici, sílu a flexibility. Mým úkolem je vás motivovat a posunout vaše fyzické schopnosti na vyšší úroveň.</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="card">
                <div className="card-image">
                  <img src="/images/vitek 3.png" alt="" />
                </div>
                <div className="card-content">
                  <h1>Jan Vítek</h1>
                  <p>Můj hlavní úkol spočívá ve sestavování jídelníčků, které nejen podpoří váš trénink, ale také vám pomohou dosáhnout vašich fitness cílů.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="dotaznik">
        <div className="page-title">
          <h1>DOTAZNÍK</h1>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="dotaznik-content">
                <h2>Vyplňte prosím krátký dotazník spokojenosti</h2>
                <p>Hodnocení jako ve škole, 1 = nejlepší, 5 = nejhorší</p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-12">
          <div className="dotaznik-form">
            <form action="#">
              <table>
                <thead>
                  <tr>
                    <th></th>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>Vybavení posilovny</th>
                    <td>
                      <input type="radio" name="vybaveni" />
                    </td>
                    <td>
                      <input type="radio" name="vybaveni" />
                    </td>
                    <td>
                      <input type="radio" name="vybaveni" />
                    </td>
                    <td>
                      <input type="radio" name="vybaveni" />
                    </td>
                    <td>
                      <input type="radio" name="vybaveni" />
                    </td>
                  </tr>
                  <tr>
                    <th>Personál</th>
                    <td>
                      <input type="radio" name="personal" />
                    </td>
                    <td>
                      <input type="radio" name="personal" />
                    </td>
                    <td>
                      <input type="radio" name="personal" />
                    </td>
                    <td>
                      <input type="radio" name="personal" />
                    </td>
                    <td>
                      <input type="radio" name="personal" />
                    </td>
                  </tr>
                  <tr>
                    <th>Kvalita Šaten</th>
                    <td>
                      <input type="radio" name="satny" />
                    </td>
                    <td>
                      <input type="radio" name="satny" />
                    </td>
                    <td>
                      <input type="radio" name="satny" />
                    </td>
                    <td>
                      <input type="radio" name="satny" />
                    </td>
                    <td>
                      <input type="radio" name="satny" />
                    </td>
                  </tr>
                  <tr>
                    <th>Komunita</th>
                    <td>
                      <input type="radio" name="komunita" />
                    </td>
                    <td>
                      <input type="radio" name="komunita" />
                    </td>
                    <td>
                      <input type="radio" name="komunita" />
                    </td>
                    <td>
                      <input type="radio" name="komunita" />
                    </td>
                    <td>
                      <input type="radio" name="komunita" />
                    </td>
                  </tr>
                  <tr>
                    <th>Ceny produktů</th>
                    <td>
                      <input type="radio" name="ceny" />
                    </td>
                    <td>
                      <input type="radio" name="ceny" />
                    </td>
                    <td>
                      <input type="radio" name="ceny" />
                    </td>
                    <td>
                      <input type="radio" name="ceny" />
                    </td>
                    <td>
                      <input type="radio" name="ceny" />
                    </td>
                  </tr>
                  <tr>
                    <th>Trenéři</th>
                    <td>
                      <input type="radio" name="coach" />
                    </td>
                    <td>
                      <input type="radio" name="coach" />
                    </td>
                    <td>
                      <input type="radio" name="coach" />
                    </td>
                    <td>
                      <input type="radio" name="coach" />
                    </td>
                    <td>
                      <input type="radio" name="coach" />
                    </td>
                  </tr>
                  <tr>
                    <th>Celkové hodnocení gymu</th>
                    <td>
                      <input type="radio" name="overall" />
                    </td>
                    <td>
                      <input type="radio" name="overall" />
                    </td>
                    <td>
                      <input type="radio" name="overall" />
                    </td>
                    <td>
                      <input type="radio" name="overall" />
                    </td>
                    <td>
                      <input type="radio" name="overall" />
                    </td>
                  </tr>
                </tbody>
              </table>
              <table>
                <tbody>
                  <tr>
                    <th>Co byste u nás zlepšili?</th>
                    <td>
                      <input className="form-input" type="text" />
                    </td>
                  </tr>
                  <tr>
                    <th>Vaše jméno (nepovinné)</th>
                    <td>
                      <input className="form-input" type="text" />
                    </td>
                  </tr>
                  <tr>
                    <th>Váš email</th>
                    <td>
                      <input className="form-input" type="text" />
                    </td>
                  </tr>
                </tbody>
              </table>
            </form>
          </div>
        </div>
      </section>
      <section id="kontakt">
        <div className="page-title">
          <h1>KONTAKT</h1>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <div className="kontakt-mapa">
                <h1>Kde nás najdete</h1>
                <p>Na Hradbách 11, 280 02, Kolín</p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2563.153430503596!2d15.197326969426921!3d50.027220776349374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470c152129c11ad1%3A0x589b45e9f550cb21!2sNa%20Hradb%C3%A1ch%2011%2C%20280%2002%20Kol%C3%ADn%202!5e0!3m2!1scs!2scz!4v1704370065446!5m2!1scs!2scz"></iframe>
              </div>
            </div>
            <div className="col-md-6">
              <div className="kontakt-content">
                <h1>Náš email</h1>
                <p>info.musilekgym@gmail.com</p>
                <h1>Telefoní číslo</h1>
                <p>+420 777 288 492</p>
              </div>
            </div>
            <div className="col-md-12">
              <div className="kontakt-dotaz">
                <h1>Máte dotaz?</h1>
                <p>Neváhejte se nás zeptat!</p>
                <Contant />
              </div>
            </div>
          </div>
        </div>
      </section>
      <section id="recenze">
        <div className="page-title">
          <h1>RECENZE</h1>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="card">
                <div className="card-header">
                  <img className="profile-pic" src="/images/Blank-profile.png" alt="" />
                  <h3>Prokop Buben</h3>
                  <div className="stars">
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-regular fa-star-half-stroke"></i>
                  </div>
                </div>
                <div className="card-content">
                  <p>Posilovna mě příjemně překvapila svou širokou nabídkou moderního vybavení a příjemnou atmosférou. Personál je vstřícný a ochotný pomoci s jakýmkoli dotazem. Vybavení je ve skvělém stavu, což zvyšuje kvalitu mého tréninku.</p>
                  <h4>1. ledna 2023</h4>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card">
                <div className="card-header">
                  <img className="profile-pic" src="/images/Blank-profile.png" alt="" />
                  <h3>Tomáš Jelínek</h3>
                  <div className="stars">
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </div>
                </div>
                <div className="card-content">
                  <p>Mám za sebou několik posiloven, ale tahle mě opravdu oslovila. Nízká cena za členství a přitom moderní vybavení? To je pro mě ideální kombinace.</p>
                  <h4>1. ledna 2023</h4>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card">
                <div className="card-header">
                  <img className="profile-pic" src="/images/Blank-profile.png" alt="" />
                  <h3>Simona Zahradníková</h3>
                  <div className="stars">
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-regular fa-star-half-stroke"></i>
                    <i class="fa-regular fa-star"></i>
                  </div>
                </div>
                <div className="card-content">
                  <p>Posilovna je pro mě jako dokonalý matematický vzorec – výrazná a komplexní. Široká škála cvičení poskytuje jako matematická rovnice různé proměnné, které mohu kombinovat a optimalizovat.</p>
                  <h4>1. ledna 2023</h4>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card">
                <div className="card-header">
                  <img className="profile-pic" src="/images/Blank-profile.png" alt="" />
                  <h3>Vít Koldinský</h3>
                  <div className="stars">
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-regular fa-star-half-stroke"></i>
                  </div>
                </div>
                <div className="card-content">
                  <p>Velmi příjemné fitness centrum. Gym navštěvuji pravidelně a již půl roku pro mě Musílek Gym je na prvním místě.</p>
                  <h4>1. ledna 2023</h4>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card">
                <div className="card-header">
                  <img className="profile-pic" src="/images/Blank-profile.png" alt="" />
                  <h3>Vilém Šťastný</h3>
                  <div className="stars">
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-regular fa-star"></i>
                  </div>
                </div>
                <div className="card-content">
                  <p>Pro mě je posilovna jako krátká zastávka na cestě, kde se setkávám s různými 'činkovými zastávkami'. I když mám v zaměstnání spíše co dočinění s kolejemi, posilovna mi poskytuje klid a sílu pro další cestu. Doporučuji ji všem, kteří hledají osobní posun a rovnováhu.</p>
                  <h4>1. ledna 2023</h4>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="card">
                <div className="card-header">
                  <img className="profile-pic" src="/images/Blank-profile.png" alt="" />
                  <h3>Karel Kopfrkingl</h3>
                  <div className="stars">
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                    <i class="fa-solid fa-star"></i>
                  </div>
                </div>
                <div className="card-content">
                  <p>Pro mě je tato posilovna jako oddechový okamžik od mé práce v krematoriu. Stejně jako v mé profesi, i zde se setkávám s respektem k tělu a péčí o něj.</p>
                  <h4>1. ledna 2023</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
}
