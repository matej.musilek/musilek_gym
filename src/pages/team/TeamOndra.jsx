import "./Team.css";
import React from "react";
import Navbar from "../../components/navbar/navbar";
import Footer from "../../components/footer/footer";

function TeamOndra() {
  return (
    <>
      <Navbar />
      <section id="team-page">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="team-content">
                <img src="/images/Group 45.png" alt="" />
                <h1>Ondřej Klér</h1>
                <p>Jsem trenér se zaměřením na silový trénink. Mým cílem je pomoci vám dosáhnout optimální fyzické kondice s důrazem na správnou techniku cvičení a individuální plánování programu.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
}

export default TeamOndra;
