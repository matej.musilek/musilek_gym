import "./Team.css";
import React from "react";
import Navbar from "../../components/navbar/navbar";
import Footer from "../../components/footer/footer";

function TeamJan() {
  return (
    <>
      <Navbar />
      <section id="team-page">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="team-content">
                <img src="/images/Group 47.png" alt="" />
                <h1>Jan Vítek</h1>
                <p>Můj hlavní úkol spočívá ve sestavování jídelníčků, které nejen podpoří váš trénink, ale také vám pomohou dosáhnout vašich fitness cílů.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
}

export default TeamJan;
