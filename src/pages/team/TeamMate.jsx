import "./Team.css";
import React from "react";
import Navbar from "../../components/navbar/navbar";
import Footer from "../../components/footer/footer";

function TeamMate() {
  return (
    <>
      <Navbar />
      <section id="team-page">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="team-content">
                <img src="/images/Group 46.png" alt="" />
                <h1>Matěj Musílek</h1>
                <p>Jsem majitel a crossfit trenér, kombinující kondici, sílu a flexibilitu. Mým úkolem je vás motivovat a posunout vaše fyzické schopnosti na vyšší úroveň.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
}

export default TeamMate;
