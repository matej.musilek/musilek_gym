import React, { useState } from "react";
import "./navbar.css";
import { Link } from "react-router-dom";
import { HashLink } from "react-router-hash-link";

export default function Navbar() {
  const [isOpen, setisOpen] = useState(false);

  return (
    <nav>
      <div className="top">
        <a href="/">
          <div className="navbar-logo">
            <img src="/images/logo.png" alt="logo" draggable="false" />
            <h1>Musílek gym</h1>
          </div>
        </a>
        <div className="menu">
          <ul className={isOpen ? "show" : "hide"}>
            <li className="nav-item">
              <HashLink className="nav-link" smooth to="/#home-sec">
                Úvod
              </HashLink>
            </li>
            <li className="nav-item">
              <HashLink className="nav-link" smooth to="/#about-page">
                O nás
              </HashLink>
            </li>
            <li className="nav-item">
              <HashLink className="nav-link" smooth to="/#gallery">
                Galerie
              </HashLink>
            </li>
            <li className="nav-item">
              <HashLink className="nav-link" smooth to="/#sluzby">
                Služby
              </HashLink>
            </li>
            <li className="nav-item">
              <HashLink className="nav-link" smooth to="/#dotaznik">
                Dotazník
              </HashLink>
            </li>
            <li className="nav-item">
              <HashLink className="nav-link" smooth to="/#kontakt">
                Kontakt
              </HashLink>
            </li>
            <li className="nav-item">
              <HashLink className="nav-link" smooth to="/#recenze">
                Recenze
              </HashLink>
            </li>
          </ul>
          {!isOpen ? <i className="fa fa-bars nav-changer" onClick={() => setisOpen(true)}></i> : <i className="fa-solid fa-xmark nav-changer" onClick={() => setisOpen(false)}></i>}
        </div>
      </div>
    </nav>
  );
}
