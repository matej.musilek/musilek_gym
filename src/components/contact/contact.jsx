import React, { useRef } from "react";
import emailjs from "@emailjs/browser";
import "./contact.css";

export const ContactUs = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm("service_a1zysxv", "template_hyhy7l9", form.current, "5PbBTv6R6ZyeunTMC").then(
      (result) => {
        console.log(result.text);
        Swal.fire({
          position: "center",
          icon: "success",
          title: "Email byl úspěšně odeslán!",
          showConfirmButton: false,
          timer: 1500,
          background: "#1d1d1d",
          color: "#fff",
        });
      },
      (error) => {
        console.log(error.text);
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Error",
          showConfirmButton: false,
          timer: 1500,
          background: "#1d1d1d",
          color: "#fff",
        });
      }
    );
  };

  return (
    <form ref={form} onSubmit={sendEmail}>
      <table>
        <tbody>
          <tr>
            <th>Vaše jméno:</th>
            <td>
              <input type="text" name="user_name" className="dotaz" />
            </td>
          </tr>
          <tr>
            <th>Vaš email:</th>
            <td>
              <input type="text" name="user_email" className="dotaz" />
            </td>
          </tr>
          <tr>
            <th>Vaš dotaz:</th>
            <td>
              <input type="text" name="message" className="dotaz" />
            </td>
          </tr>
        </tbody>
      </table>
      <div className="button">
        <input type="submit" value="Send" className="btn" />
      </div>
    </form>
  );
};

export default ContactUs;
