import "./footer.css";
export default function Footer() {
  return (
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="footer-content">
              <img src="/images/logo.png" alt="" />
              <h1>MUSÍLEK GYM</h1>
              <p>© 2024 Musílek Gym. Všechna práva vyhrazena. Provozováno s láskou k posilování a zdravému životnímu stylu.</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
